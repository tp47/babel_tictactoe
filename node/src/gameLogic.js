// updates the game when a player makes a move
export const updateBoard = (player, location, oldBoard) => {
    let newBoard = [...oldBoard];
    newBoard[location] = player;
    return newBoard;
}


export const calculateWinner = (squares) => {
    const lines = [
        [0, 1, 2], [3, 4, 5], [6, 7, 8], [0, 3, 6],
        [1, 4, 7], [2, 5, 8], [0, 4, 8], [2, 4, 6],
    ];
    for (var line of lines) {
        const [a, b, c] = line;
        if (squares[a] &&
            squares[a] === squares[b] &&
            squares[a] === squares[c])
            return squares[a];
    }
    return null;
};

export const makeMove = (currentGameState, loc) => {
    let squares = [...currentGameState.squares];
    let player = currentGameState.xIsNext? 'X' : 'O';
    let xIsNext = !currentGameState.xIsNext;
    let moves = currentGameState.moves;
    if(!squares[loc] && loc >= 0 && loc <= 8){
		squares = updateBoard(player, loc, currentGameState.squares);
		moves = moves + 1;
    }
    else
		console.log("\nInvalid input, try again");

	return {
		squares,
		xIsNext,
		moves
	};
};
