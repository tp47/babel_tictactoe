import {makeMove, calculateWinner} from './gameLogic.js';

const evaluteSquares = (squares) => {
    switch(calculateWinner(squares)) {
        case 'X':
          return -1000; // Human wins
        case 'O':
          return 1000; // CPU wins
        default:
          return 0; // No one wins
    }
};

const maximize = (val1, val2) => (val1[1] > val2[1]) ? val1 : val2;
const minimize = (val1, val2) => (val1[1] > val2[1]) ? val2 : val1;
const getAvailable = (squares) =>
      [...squares.keys()].filter(i => squares[i] === null);

const search = (gameState) => {
    const {squares, xIsNext, moves} = gameState;
    const gameScore = evaluteSquares(squares);
    if (moves === 9 || gameScore !== 0) {
        return [-1, gameScore];
    }

    const operation = xIsNext ? minimize: maximize;
    let value = xIsNext ? [-1, Infinity] : [-1, -Infinity];
    for (var position of getAvailable(squares)) {
        const tempState = makeMove(gameState, position);
        let result = search(tempState);
        result[0] = position;
        value = operation(value, result);
    }
    return value;
};

const bestPosition = (gameState) => search(gameState)[0];
const cpuMove = (gameState) => makeMove(gameState, bestPosition(gameState));
export default cpuMove;
