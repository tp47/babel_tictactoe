import cpuMove from './minimax.js';
import {updateBoard, makeMove, calculateWinner} from './gameLogic.js';
import {question} from 'readline-sync';

// Helpers
const consoleOutput= (s) => process.stdout.write(s);
const getRandom = (lst) =>
    lst[Math.floor((Math.random()*lst.length))];

const fullClear = () => consoleOutput('\x1b[2J');
const semiClear = () => consoleOutput('\x1b[0f');

const newGameState = {
    squares: Array(9).fill(null),
    xIsNext: getRandom([true, false]),
    moves: 0,
};

const formatValue = (value) => {
    if (value === null) {
	return " ";
    }
    return value;
}
const printGame = (currentGameState) => {
    semiClear();
    const formatted_board = () => {
	const formatted_game = currentGameState.squares.map(formatValue);
	let i = 0;
	for (let cell of formatted_game){	    
	    switch(i){
	    case 2:
	    case 5:
		consoleOutput(cell + "\n");
		consoleOutput("--------\n");
		break;

	    case 8:
		consoleOutput(cell + "\n\n\n\n");
		break;
	    default:
		consoleOutput(cell + " |");
	    }
	    i = i + 1;
	 
	}
    };
    formatted_board();
};

// Where all the action happens
function gameLoop(currentGameState) {
    printGame(currentGameState);
    let winner = calculateWinner(currentGameState.squares);

    if (winner || currentGameState.moves === 9)
	return winner;

    if (!currentGameState.xIsNext) {
	const newGameState = cpuMove(currentGameState);
	return gameLoop(newGameState);
    }
    // If it's the human's turn...
    const loc_str = question(`Next move? `);
    fullClear();
    const loc = parseInt(loc_str, 10) - 1;

    if (loc < 0 || loc > 8)
	fullClear(); // invalid inputs are very long, so need a full clear
    
    const newGameState = makeMove(currentGameState, loc);
    
    if (newGameState.moves !== currentGameState.moves)
    	return gameLoop(newGameState);
    else
    	return gameLoop(currentGameState);
}

// Entry point to the game
function main(){
    fullClear();
    const winner = gameLoop(newGameState);
    if(winner === null)
	console.log("The game was a draw");
    else {
	console.log("The game was won by", winner);
    }
}



main();
